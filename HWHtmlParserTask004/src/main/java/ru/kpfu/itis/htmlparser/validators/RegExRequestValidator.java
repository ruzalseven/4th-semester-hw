package ru.kpfu.itis.htmlparser.validators;

import org.springframework.validation.Errors;
import org.springframework.validation.ValidationUtils;
import org.springframework.validation.Validator;
import ru.kpfu.itis.htmlparser.models.Request;

import java.net.MalformedURLException;
import java.net.URL;

public class RegExRequestValidator implements Validator {

    @Override
    public boolean supports(Class clazz) {
        return Request.class.equals(clazz);
    }

    @Override
    public void validate(Object obj, Errors e) {
        ValidationUtils.rejectIfEmpty(e, "url", "URL.empty");
        ValidationUtils.rejectIfEmpty(e, "expression", "EXPRESSION.empty");
        Request request = (Request) obj;
        if (request.getUrl().isEmpty() || request.getExpression().isEmpty()) return;
        try {
            new URL(request.getUrl());

        } catch (MalformedURLException ex) {
            e.rejectValue("url", "URL.incorrect");
        }
    }
}
