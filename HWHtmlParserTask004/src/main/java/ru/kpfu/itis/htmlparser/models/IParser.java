package ru.kpfu.itis.htmlparser.models;

import org.xml.sax.SAXException;

import javax.xml.parsers.ParserConfigurationException;
import javax.xml.xpath.XPathExpressionException;
import java.io.IOException;
import java.net.URL;
import java.util.Collection;

/**
 * Created by RYumaev on 14.03.2015.
 */
public interface IParser {
    String parse(URL url, String expression) throws ParserConfigurationException, XPathExpressionException, IOException;
}
