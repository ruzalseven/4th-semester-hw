package ru.kpfu.itis.htmlparser.models;

import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import java.net.URLConnection;
import java.nio.charset.Charset;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.regex.PatternSyntaxException;

/**
 * Created by RYumaev on 14.03.2015.
 */
public class RegExParser implements IParser {
    byte[] bytes = new byte[1024];

    @Override
    public String parse(URL url, String expression) throws IOException, PatternSyntaxException {
        URLConnection connection = url.openConnection();
        InputStream inputStream = connection.getInputStream();
        String page = "";
        int c = inputStream.read(bytes);
        while (c != -1) {
            page += new String(bytes, 0, c, connection.getContentEncoding() != null ? Charset.forName(connection.getContentEncoding()) : Charset.forName("UTF-8"));
            c = inputStream.read(bytes);
        }
        // page=new String(page.getBytes(),Charset.forName("UTF-8"));
        Pattern pattern = Pattern.compile(expression);
        Matcher matcher = pattern.matcher(page);
        String results = "";
        while (matcher.find()) {
            results += getInnerData(expression, matcher.group()) + "\n";
        }
        return results;
    }

    private static String getInnerData(String expression, String value) {
        int start = expression.indexOf("(");
        char last = expression.charAt(expression.length() - 1);
        if (last == ')' || (last == '*' || last == '+') && expression.charAt(expression.length() - 2) == ')') {
            Pattern pattern = Pattern.compile(expression.substring(0, start));
            Matcher matcher = pattern.matcher(value);
            while (matcher.find()) {
                value = value.replace(matcher.group(), "");
            }
        }
        return value;

    }


}
