package ru.kpfu.itis.htmlparser.models;

import org.htmlcleaner.CleanerProperties;
import org.htmlcleaner.DomSerializer;
import org.htmlcleaner.HtmlCleaner;
import org.htmlcleaner.TagNode;
import org.w3c.dom.Document;
import org.w3c.dom.NamedNodeMap;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import javax.xml.parsers.ParserConfigurationException;
import javax.xml.xpath.*;
import java.io.IOException;
import java.net.URL;

/**
 * Created by RYumaev on 14.03.2015.
 */
public class XpathParser implements IParser {

    @Override
    public String parse(URL url, String expression) throws IOException, ParserConfigurationException, XPathExpressionException {
        HtmlCleaner htmlCleaner = new HtmlCleaner();
        TagNode tagNode = htmlCleaner.clean(url);
        Document doc = new DomSerializer(
                new CleanerProperties()).createDOM(tagNode);
        XPathFactory factory = XPathFactory.newInstance();
        XPath xpath = factory.newXPath();
        XPathExpression expr = xpath.compile(expression);
        Object result = expr.evaluate(doc, XPathConstants.NODESET);
        NodeList nodes = (NodeList) result;
        String results = "";
        for (int i = 0; i < nodes.getLength(); i++) {
            Node node = nodes.item(i);
            NodeList childs = node.getChildNodes();
            String childsS = "";
            if (childs != null) childsS = getChilds(childs);
            results += node.getNodeName()+"\n\t"+ getAttributes(node.getAttributes())  + (node.getNodeValue() != null ? node.getNodeValue() : "") + childsS +node.getNodeName()+"\n";
        }
        return results;
    }


    private static String getChilds(NodeList nodes) {
        String result = "";
        for (int i = 0; i < nodes.getLength(); i++) {
            Node node = nodes.item(i);
            NodeList childs = node.getChildNodes();
            String childsS = "";
            //if(node.getNodeName().compareTo("#text")==0) continue;
            if (childs != null) childsS = getChilds(childs);
            result +=(node.getNodeName().compareTo("#text") != 0 ?  node.getNodeName()+"\n\t" + getAttributes(node.getAttributes()) : "") + (node.getNodeValue() != null ? node.getNodeValue() : "") + childsS+"\n" + (node.getNodeName().compareTo("#text") != 0 ? node.getNodeName()+"\n": "");
        }

        return result;
    }

    private static String getAttributes(NamedNodeMap namedNodeMap) {
        String result = "";
        if (namedNodeMap != null) {
            for (int i = 0; i < namedNodeMap.getLength(); i++) {
                result += " " + namedNodeMap.item(i).getNodeName() + "=\"" + namedNodeMap.item(i).getTextContent() + "\"";
            }
        }

        return result;
    }

}
