package ru.kpfu.itis.htmlparser.models;

import javax.xml.parsers.ParserConfigurationException;
import javax.xml.xpath.XPathExpressionException;
import java.io.IOException;
import java.net.URL;

/**
 * Created by RYumaev on 14.03.2015.
 */
public class Request {

    private String url;

    public Boolean getOnlyParam() {
        return onlyParam;
    }

    public void setOnlyParam(Boolean onlyParam) {
        this.onlyParam = onlyParam;
    }

    private Boolean onlyParam = false;

    public String getExpression() {
        return expression;
    }

    public void setExpression(String expression) {
        this.expression = expression;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    private String expression;

    public String getResult(IParser parser) throws IOException, ParserConfigurationException, XPathExpressionException {
        return parser.parse(new URL(url), expression);

    }
}
