package ru.kpfu.itis.htmlparser.controllers;

import org.apache.commons.lang.StringEscapeUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;
import ru.kpfu.itis.htmlparser.models.RegExParser;
import ru.kpfu.itis.htmlparser.models.Request;
import ru.kpfu.itis.htmlparser.models.XpathParser;
import ru.kpfu.itis.htmlparser.validators.XPathRequestValidator;

import javax.xml.parsers.ParserConfigurationException;
import javax.xml.xpath.XPathExpressionException;
import java.io.IOException;

@Controller
@RequestMapping("/xpath")
public class XPathController {
    @Autowired
    XpathParser xpathParser;
    @InitBinder
    private void initBinder(WebDataBinder binder) {
        binder.setValidator(new XPathRequestValidator());
    }

    @RequestMapping(value = {"/", ""}, method = RequestMethod.GET)
    public String index(ModelMap map) {
        map.addAttribute("request", new Request());
        return "parse";
    }

    @RequestMapping(value = {"/", ""}, method = RequestMethod.POST)
    public String index(
            RedirectAttributes redirectAttributes,
            @Validated Request request,
            BindingResult result,
            ModelMap map) {
        if (result.hasErrors()) {
            return "parse";
        } else {
            try {
                String res = request.getResult(xpathParser);
                map.put("resultCode", StringEscapeUtils.escapeHtml(res));
                //  map.put("resultHTML",res);
            } catch (IOException e) {
                result.rejectValue("url", "URL.connection.error");
            } catch (ParserConfigurationException | XPathExpressionException e) {
                result.rejectValue("expression", "EXPRESSION.incorrect");
            }
            return "parse";
        }
    }


}