<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<html>
<head>
    <link rel="stylesheet" href="/resources/js/styles/default.css">
    <script src="/resources/js/highlight.pack.js"></script>
    <script>hljs.initHighlightingOnLoad();</script>

    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
</head>
<body>
<%@page contentType="text/html" pageEncoding="UTF-8" %>
<form:form method="post" commandName="request">
    <form:label path="url">Url to parse: </form:label>
    <form:input path="url" cssStyle="margin-bottom: 15px"/>
    <form:errors path="url" cssStyle="color: red;" element="label"/></br>
    <form:label path="expression">Expression to parse: </form:label>
    <form:input path="expression" cssStyle="margin-bottom: 15px"/>
    <form:errors path="expression" cssStyle="color: red;" element="label"/></br>
    <input type="submit"/>
</form:form>
<c:if test="${not empty resultCode}">
    <h1>Result Code</h1>
    <pre><code class="html">
            ${resultCode}</code></pre>

</c:if>

<c:if test="${not empty resultHTML}">
    <h1>Result HTML</h1>

    <div>
            ${resultHTML}
    </div>
</c:if>

</body>
</html>