package ru.kpfu.bookstore;

import org.springframework.validation.Errors;
import org.springframework.validation.ValidationUtils;
import org.springframework.validation.Validator;

import java.net.MalformedURLException;
import java.net.URL;

public class AuthorValidator implements Validator {

    @Override
    public boolean supports(Class clazz) {
        return Author.class.equals(clazz);
    }

    @Override
    public void validate(Object obj, Errors e) {
        ValidationUtils.rejectIfEmpty(e, "name", "name.empty");
        Author a = (Author) obj;
        try {
            new URL(a.getAvatarUrl());
        } catch (MalformedURLException ex) {
            e.rejectValue("avatarUrl", "URL.incorrect");
        }
    }
}
