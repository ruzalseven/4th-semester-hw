package ru.kpfu.itis.homework;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Bean;

/**
 * Created by Ruzal on 15.02.2015.
 */
@Configuration
@ComponentScan
public class ConfigApp {

    @Bean
    public String textSource() {
        return "Text example";
    }

}
