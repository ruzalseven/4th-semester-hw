package ru.kpfu.itis.homework;

import org.springframework.stereotype.Component;

/**
 * Created by Ruzal on 15.02.2015.
 */
@Component
public class UpperPrinter extends Printer {
    @Override
    public void print() {
        System.out.println(textSource.toUpperCase());
    }
}
