package ru.kpfu.itis.homework;

import org.springframework.stereotype.Component;

/**
 * Created by Ruzal on 15.02.2015.
 */
@Component
public class LowerPrinter extends Printer {
    @Override
    public void print() {
        System.out.println(textSource.toLowerCase());
    }
}
