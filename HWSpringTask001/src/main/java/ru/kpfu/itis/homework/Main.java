package ru.kpfu.itis.homework;

import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;

public class Main {
    public static void main(String[] args) {
        ApplicationContext context = new AnnotationConfigApplicationContext(ConfigApp.class);
        IPrinter upperPrinter = (IPrinter) context.getBean("upperPrinter");
        Printer lowerPrinter = (Printer) context.getBean("lowerPrinter");
        upperPrinter.print();
        lowerPrinter.print();
    }
}
