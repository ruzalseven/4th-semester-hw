package com.company;

import java.util.ArrayList;

/**
 * Created by RYumaev on 26.04.2015.
 */
public class Graf {
    public Vertex getVertexByName(long name) {
        for (Vertex v : vertexes) {
            if (v.getName() == name) return v;
        }
        return null;
    }

    public ArrayList<Vertex> getVertexes() {
        return vertexes;
    }

    public void setVertexes(ArrayList<Vertex> vertexes) {
        this.vertexes = vertexes;
    }

    private ArrayList<Vertex> vertexes = new ArrayList<>();

    public Graf(int countV) {
        vertexes = new ArrayList<>(countV);
        for (int i = 0; i < countV; i++) {
            vertexes.add(new Vertex(i + 1));
        }
    }

    public Graf() {
    }

    public void addByName(Vertex vertex) {
        vertexes.add(vertex);
    }

    public void addVertex(Vertex vertex) {
        vertexes.add(vertex);
    }

    public void addEdge(int a, int b) {
        vertexes.get(a - 1).getVertexes().add(vertexes.get(b - 1));
        vertexes.get(b - 1).getVertexes().add(vertexes.get(a - 1));
    }

    @Override
    public String toString() {
        return "Graf{" +
                "vertexes=" + vertexes.toString() +
                '}';
    }

    public void deleteEdge(int a, int b) {
        vertexes.get(a - 1).getVertexes().remove(vertexes.get(b - 1));
        vertexes.get(b - 1).getVertexes().remove(vertexes.get(a - 1));
    }

    public ArrayList<Vertex> getMinVertexs() {
        int min = 0;
        ArrayList<Vertex> result = new ArrayList<>();
        for (int i = 0; i < vertexes.size(); i++) {
            if (vertexes.get(i).getVertexes().size() < vertexes.get(min).getVertexes().size()) {
                min = i;
                result = new ArrayList<>();
            }
            if (vertexes.get(i).getVertexes().size() == vertexes.get(min).getVertexes().size()) {
                result.add(vertexes.get(i));
            }
        }
        return result;
    }

    public static Graf copy(Graf graf) {
        Graf g = new Graf();
        graf.getVertexes().forEach((Vertex v) -> g.getVertexes().add(Vertex.copy(v)));
        for (Vertex a : graf.getVertexes()) {
            for (Vertex b : a.getVertexes()) {
                g.getVertexByName(a.getName()).getVertexes().add(g.getVertexByName(b.getName()));
                g.getVertexByName(b.getName()).getVertexes().add(g.getVertexByName(a.getName()));
            }
        }
        return g;
    }

    public boolean existVertex(Vertex vertex) {
        for (Vertex v : vertexes) {
            if (v.getName() == vertex.getName()) return true;
        }
        return false;
    }

    public void removeEdge(int a, int b) {
        Vertex aV = null;
        Vertex bV = null;
        for (Vertex v : vertexes) {
            if (v.getName() == a) {
                aV = v;
            }
            if (v.getName() == b) {
                bV = v;
            }
        }
        aV.getVertexes().remove(b);
        bV.getVertexes().remove(a);
    }

}