package com.company;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.Scanner;
import java.util.Set;

public class Main {
    private static ArrayList<Graf> results = new ArrayList<>();

    public static void main(String[] args) {
        Graf graf = gen(4);
        seachOstov(null, graf);
        for (Graf graf1 : results) {
            System.out.println(graf1);
        }
        System.out.println(results.size());
    }
    private static Graf gen(int n) {
        Graf g = new Graf(n);
        for (int i = 1; i <= n; i++) {
            for (int j = 1; j <= n; j++) {
                if (i < j) g.addEdge(i, j);
            }
        }
        return g;
    }

    public static Graf getGrafFormInput() {
        Scanner scanner = new Scanner(System.in);
        int countV = Integer.parseInt(scanner.nextLine());
        Graf graf = new Graf(countV);
        String line = scanner.nextLine();
        while (line.compareTo("end") != 0) {
            int i = line.indexOf('-');
            int a = Integer.parseInt(line.substring(0, i));
            int b = Integer.parseInt(line.substring(i + 1));
            graf.addEdge(a, b);
            line = scanner.nextLine();
        }
        return graf;
    }

    public static void seachOstov(Graf preGraf, Graf subGraf) {
        System.out.println("PREF " + preGraf);
        System.out.println("SUB " + subGraf);
        Graf result = preGraf;
        if (preGraf == null) {
            result = new Graf();
        }

        for (Vertex v : subGraf.getMinVertexs()) {
            if (v.getVertexes().size() == 1) {
                Vertex a = null;
                if (result.existVertex(v)) {
                    a = result.getVertexByName(v.getName());
                } else {
                    a = Vertex.copy(v);
                    result.addVertex(a);
                }
                Vertex v2 = v.getVertexes().iterator().next();
                Vertex b = null;
                if (result.existVertex(v2)) {
                    b = result.getVertexByName(v2.getName());
                } else {
                    b = Vertex.copy(v2);
                    result.addVertex(b);
                }
                a.getVertexes().add(b);
                b.getVertexes().add(a);
                v.getVertexes().remove(v2);
                v2.getVertexes().remove(v);
                if (v.getVertexes().size() == 0) subGraf.getVertexes().remove(v);
                if (v2.getVertexes().size() == 0) subGraf.getVertexes().remove(v2);
            }
        }

        for (Vertex v : subGraf.getMinVertexs()) {
            if (v.getVertexes().size() > 1) {
                if (!v.isVisited()) {
                    for (Vertex subV : v.getVertexes()) {
                        if (!subV.isVisited()) {
                            Graf nextSub = Graf.copy(subGraf);
                            Vertex a = nextSub.getVertexByName(v.getName());
                            Vertex b = nextSub.getVertexByName(subV.getName());
                            a.getVertexes().remove(b);
                            b.getVertexes().remove(a);
                            seachOstov(Graf.copy(result), nextSub);
                        }
                    }
                }
                v.setVisited(true);
            }
        }
        if (subGraf.getVertexes().size() == 0) results.add(result);
        System.out.print("\n\t");
    }
}

