package com.company;

import java.util.HashSet;
import java.util.Set;

/**
 * Created by RYumaev on 26.04.2015.
 */

public class Vertex {
    private long name;
    private Set<Vertex> vertexes = new HashSet<>();

    public boolean isVisited() {
        return visited;
    }

    public void setVisited(boolean visited) {
        this.visited = visited;
    }


    private boolean visited = false;

    public Vertex(long name) {
        this.name = name;
    }

    @Override
    public String toString() {
        final String[] str = {""};
        vertexes.forEach((Vertex v) -> str[0] += " " + v.getName());

        return "Vertex{ name=" + name +
                " vertexes=" + str[0] +
                '}';
    }

    public Set<Vertex> getVertexes() {
        return vertexes;
    }

    public void setVertexes(Set<Vertex> vertexes) {
        this.vertexes = vertexes;
    }

    public long getName() {
        return name;
    }

    public void setName(long name) {
        this.name = name;
    }

    public static Vertex copy(Vertex vertex) {
        Vertex vertex1 = new Vertex(vertex.getName());
        vertex1.setVisited(vertex.isVisited());
        return vertex1;

    }

}

