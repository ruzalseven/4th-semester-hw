package sample;

import com.tunyk.currencyconverter.api.CurrencyConverterException;

/**
 * Created by Ruaal on 21.02.2015.
 */
public interface IConverter {
    public double convert(float value, String from, String to) throws CurrencyConverterException;

    public String[] getCurrency();
}
