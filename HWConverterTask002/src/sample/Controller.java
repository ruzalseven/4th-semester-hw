package sample;

import com.tunyk.currencyconverter.api.CurrencyConverterException;
import javafx.event.ActionEvent;
import javafx.event.Event;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.scene.control.Label;
import javafx.scene.control.MenuItem;
import javafx.scene.control.SplitMenuButton;
import javafx.scene.control.TextField;

public class Controller {
    @FXML
    private SplitMenuButton fromSplitMenu;
    @FXML
    private SplitMenuButton toSplitMenu;
    @FXML
    private TextField fromField;
    @FXML
    private TextField toField;
    @FXML
    private Label errorLabel;

    private IConverter converter;

    public void setConverter(IConverter converter) {
        this.converter = converter;
        init();
    }

    private void init() {
        toField.setText("0.0");
        fromField.setText("0.0");

        final String[] currency = converter.getCurrency();
        for (int i = 0; i < currency.length; i++) {
            MenuItem m1 = new MenuItem(currency[i]);
            final int finalI = i;
            m1.setOnAction(new EventHandler<ActionEvent>() {
                @Override
                public void handle(ActionEvent event) {
                    fromSplitMenu.setText(currency[finalI]);
                    convertFrom(event);
                }
            });

            MenuItem m2 = new MenuItem(currency[i]);
            m2.setOnAction(new EventHandler<ActionEvent>() {
                @Override
                public void handle(ActionEvent event) {
                    toSplitMenu.setText(currency[finalI]);
                    convertFrom(event);
                }
            });
            fromSplitMenu.getItems().add(m1);
            toSplitMenu.getItems().add(m2);
        }

        fromSplitMenu.setText(currency[0]);
        toSplitMenu.setText(currency[0]);
    }

    public void convertFrom(Event event) {
        errorLabel.setText("");
        float value = 0.0f;
        try {
            value = Float.parseFloat(fromField.getText());
        } catch (NumberFormatException e) {
            errorLabel.setText("incorrect input");
            toField.setText("0.0");
            fromField.setText("0.0");
        }

        try {
            toField.setText(String.valueOf(converter.convert(value, fromSplitMenu.getText(), toSplitMenu.getText())));
        } catch (CurrencyConverterException e) {
            errorLabel.setText(e.getMessage());
        }
    }

    public void convertTo(Event event) {
        errorLabel.setText("");
        float value = 0.0f;
        try {
            value = Float.parseFloat(toField.getText());
        } catch (NumberFormatException e) {
            errorLabel.setText("incorrect input");
            toField.setText("0.0");
            fromField.setText("0.0");
        }

        try {
            fromField.setText(String.valueOf(converter.convert(value, toSplitMenu.getText(), fromSplitMenu.getText())));
        } catch (CurrencyConverterException e) {
            errorLabel.setText(e.getMessage());
        }
    }

}
