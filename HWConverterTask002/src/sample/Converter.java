package sample;

import com.tunyk.currencyconverter.BankUaCom;
import com.tunyk.currencyconverter.api.Currency;
import com.tunyk.currencyconverter.api.CurrencyConverter;
import com.tunyk.currencyconverter.api.CurrencyConverterException;

/**
 * Created by Ruaal on 21.02.2015.
 */
public class Converter implements IConverter {
    private String[] currency;
    CurrencyConverter currencyConverter;
    public Converter(){
        try {
            currencyConverter=new BankUaCom(Currency.USD, Currency.EUR);
        } catch (CurrencyConverterException e) {
            e.printStackTrace();
        }
    }

    @Override
    public double convert(float value, String from, String to) throws CurrencyConverterException {
        return currencyConverter.convertCurrency(value, Currency.fromString(from), Currency.fromString(to));
    }

    @Override
    public String[] getCurrency() {
        if (currency == null) {
            Currency[] a = Currency.values();
            currency = new String[a.length];
            for (int i = 0; i < a.length; i++) {
                currency[i] = a[i].name();
            }
        }
        return currency;
    }
}
